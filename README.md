##General Info:
EC2 instance name: FreshyFresh
  instance id: i-09eb1acec0cf7a8f1

Security group name: launch-wizard-1

Public DNS: ec2-13-59-235-226.us-east-2.compute.amazonaws.com
Public IP: 13.59.235.226

MySQL passwork: freshworks

##Start up process:
Download ssh key, then run:
  `chmod 400 Freshworksbackendtest.pem`

Log in to Amazon EC2 instance from terminal:
  `ssh -i "Freshworksbackendtest.pem" ec2-user@ec2-13-59-235-226.us-east-2.compute.amazonaws.com`
Log out: exit

Transfer files from local to EC2 instance:
  `scp -i Freshworksbackendtest.pem Samplefile.txt ec2-user@ec2-13-59-235-226.us-east-2.compute.amazonaws.com:~`
  (replace Samplefile with file to be transferred; ~/var/www/html is site root?)

***
###To install and start the LAMP web server on Amazon Linux:
  1. Update instance software:
    `sudo yum update -y`

  2. Install Apache web server, MySQL, and PHP software packages:
    `sudo yum install -y httpd24 php56 mysql56-server php56-mysqlnd`

  3. Start Apache web server
    `sudo service httpd start`

  4. Configure Apache web server
    `sudo chkconfig httpd on`

    Check current configuration
    `chkconfig --list httpd`

  5. Go to Management console. Update security group to accept all incoming traffic. In instance console find Public IP (see above). Enter IP in browser.

***
###Update file permissions:
  Add user ec2-user to apache group
    `sudo usermod -a -G apache ec2-user`
    (now exit and log back in)

  Confirm existing groups
    `groups`
    (should see 'ec2-user wheel apache')

  Change ownership of var/www folder to apache group
    `sudo chown -R ec2-user:apache /var/www`

  Change the directory permissions of /var/www and its subdirectories to add group write permissions and to set the group ID on future subdirectories.
    `sudo chmod 2775 /var/www`
    `find /var/www -type d -exec sudo chmod 2775 {} \;`

  Recursively change the file permissions of /var/www and its subdirectories to add group write permissions.
    `find /var/www -type f -exec sudo chmod 0664 {} \;`

***
###Initialize SQL server:
  Start MySQL server
    `sudo service mysqld start`

  Run mysql_secure_installation
    `sudo mysql_secure_installation`

  Set MySQL to start every boot
    `sudo chkconfig mysqld on`




